package com.zoho.logs;
//$Id$

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;

@SuppressWarnings({ "all" })
public class LogsUtil {

	private static int totalNumFound;
	private static LogsUtil logsUtil = null;

	public static LogsUtil getInstance() {
		if (logsUtil == null) {
			logsUtil = new LogsUtil();
		}
		return logsUtil;
	}

	private LogsUtil() {
	}

	public JSONObject getZohoLogs(String url, String date, String searchString, String starting, String ending)
			throws Exception {

		int startRange = 1;
		int endRange = 1000;
		long start, end;
		int total;
		JSONObject logsResult = getLogs(date, 1, 1, searchString);

		JSONObject fetchLogs = new JSONObject();

		JSONObject finalLogs = new JSONObject();
		JSONObject array[] = new JSONObject[1000];
		JSONArray jArr = new JSONArray();
		int d = 0;

		JSONArray keys = logsResult.names();

		for (int i = 0; i < keys.length(); ++i) {

			String key = keys.getString(i);
			if (key.equalsIgnoreCase("numfound")) {
				int numFound = (Integer) logsResult.get(key);
				// int numFound = Integer.parseInt(value);
				if (numFound < 1000) {
					fetchLogs = getLogs(date, 1, numFound, searchString);
					return fetchLogs;

				} else {

					while (endRange <= numFound) {
						start = System.currentTimeMillis();
						array[d] = getLogs(date, startRange, endRange, searchString);
						end = System.currentTimeMillis();
						total = (int) ((end - start) * 0.001);
						d++;

						startRange = endRange + 1;
						endRange = endRange + 1000;
					}

					if (startRange < numFound) {
						start = System.currentTimeMillis();
						array[d] = getLogs(date, startRange, numFound, searchString);
						end = System.currentTimeMillis();
						total = (int) ((end - start) * 0.001);
						d++;

					}

				}

				break;

			}

		}
		for (int j = 0; j < d; j++) {
			JSONArray one = array[j].getJSONArray("docs");

			for (int k = 0; k < one.length(); k++) {
				jArr.put(one.getJSONObject(k));
			}

		}
		finalLogs.put("docs", jArr);
		return finalLogs;

	}

	public JSONObject topFiveUsersTable(String date, String searchString, String starting, String ending)
			throws Exception {

		JSONObject logsResult = getLogs(date, 1, 1, searchString);
		return logsResult;

	}

	public int getNumFound(String url, String date, String searchString, String starting, String ending)
			throws Exception {

		JSONObject logsResult = getLogs(date, 1, 1, searchString);
		JSONArray keys = logsResult.names();

		for (int i = 0; i < keys.length(); ++i) {

			String key = keys.getString(i);
			if (key.equalsIgnoreCase("numfound")) {
				int numFound = (Integer) logsResult.get(key);
				// int numFound = Integer.parseInt(value);
				totalNumFound = numFound;
			}
		}

		return totalNumFound;

	}

	public JSONObject getLogs(String date, int fromIndex, int toIndex, String searchQuery) throws Exception {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		SSLContext sc = SSLContext.getInstance("SSL");

		sc.init(null, trustAllCerts, new java.security.SecureRandom());

		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		HostnameVerifier allHostsValid = new HostnameVerifier() {

			public boolean verify(String hostname, SSLSession session) {

				return true;
			}

		};

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		URL logServer = new URL("http://www.google.com");
		URLConnection yc = logServer.openConnection();
		yc.setDoInput(true);
		yc.setDoOutput(true);

		BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		String inputLine;
		StringBuilder sbuff = new StringBuilder();

		while ((inputLine = in.readLine()) != null) {
			sbuff.append(inputLine);
		}

		JSONObject jobj = new JSONObject(sbuff.toString());

		return jobj;
	}

}
