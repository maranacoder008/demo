package com.zoho.logs;

import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.mail.smtp.SMTPTransport;

@SuppressWarnings({ "all" })
public class AutomatedParsing {

	private String url;
	private String query;
	private String folderCreateQuery;
	private String folderCreateFormat[];
	private String folderCreateMessage;
	private String massMailQuery;
	private String massMailMessage;
	private String topUsersNewMailQuery;
	private String topUsersMessage;
	private String mailWithoutContentQuery;
	private String mailWithoutContentQuery2a;
	private String mailWithoutContentQuery2b;

	private String mailWithoutContentMessage;

	private String seTypeChangeQuery;
	private String seTypeChangeMessage;
	private String topUsersFailedQuery;
	private String topUsersFailedMessage;

	private String filePath;
	private String twoDayBeforeDateFromFile;
	private String oneDayBeforeDateFromFile;
	private String startTime;
	private String endTime;
	private String crmMailSearchDateAndcount;
	private int crmMailSearchOldExceptionsCount;
	private String crmMailSearchOldExceptions;
	private int crmMailSearchNewExceptionsCount;
	private String crmMailSearchNewExceptions;
	private LogsUtil logsUtil;
	private int totalExceptionInLogServer;
	private Map<String, Integer> mailDomainMap = new HashMap<String, Integer>();
	private StringBuilder inboxMailDomainMessage = new StringBuilder();

	public static void main(String args[]) throws Exception {
		AutomatedParsing automatedParsing = new AutomatedParsing();
		automatedParsing.setInstance();
		automatedParsing.getTwoDayBeforeLogs();
	}

	public void setInstance() throws Exception {
		try {
			logsUtil = LogsUtil.getInstance();
			File logsConstants = new File("/.txt");
			Scanner fromFile = new Scanner(logsConstants);

			while (fromFile.hasNextLine()) {
				String lineOfFile = fromFile.nextLine().toString();
				String splitLine[] = lineOfFile.split("===");
				if (splitLine[0].equalsIgnoreCase("query")) {
					query = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("filepath")) {
					filePath = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("URL")) {
					url = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("FOLDERCREATEQUERY")) {
					folderCreateQuery = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("FOLDERCREATEFORMAT")) {
					folderCreateFormat = splitLine[1].split(",");
				} else if (splitLine[0].equalsIgnoreCase("MASSMAILQUERY")) {
					massMailQuery = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("TOPUSERSNEWMAILQUERY")) {
					topUsersNewMailQuery = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("MAILWITHOUTCONTENTQUERY")) {
					mailWithoutContentQuery = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("MAILWITHOUTCONTENTQUERY2a")) {
					mailWithoutContentQuery2a = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("MAILWITHOUTCONTENTQUERY2b")) {
					mailWithoutContentQuery2b = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("starttime")) {
					startTime = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("endtime")) {
					endTime = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("twodaybeforedate")) {
					twoDayBeforeDateFromFile = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("onedaybeforedate")) {
					oneDayBeforeDateFromFile = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("SETYPECHANGEQUERY")) {
					seTypeChangeQuery = splitLine[1];
				} else if (splitLine[0].equalsIgnoreCase("TOPUSERSFAILED")) {
					topUsersFailedQuery = splitLine[1];
				}
			}

			fromFile.close();
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * checks if the two day before logs are stored in a file/not. if yes,it
	 * takes the exceptions from file and stores it in hashmap and sends to
	 * onedayb4logs else,it takes the logs from server and sends the hashmap to
	 * onedayb4logs.
	 */
	public void getTwoDayBeforeLogs() throws Exception {

		String twoDayBeforeDate = "";
		if (twoDayBeforeDateFromFile.equals("0")) {
			twoDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.twoDaySubtractor);
		} else {
			twoDayBeforeDate = twoDayBeforeDateFromFile;
		}
		String date[] = twoDayBeforeDate.split("/");
		String twoDayBeforeFileName = date[0] + date[1] + date[2];

		File f = new File(filePath + twoDayBeforeFileName + ".txt");
		if (f.exists() && !f.isDirectory()) {

			HashMap<String, Integer> twoDayBeforeExceptionsMap = new HashMap<String, Integer>();

			Scanner fromFile = new Scanner(f);

			while (fromFile.hasNextLine()) {
				String lineOfFile = fromFile.nextLine().toString();
				String splitLine[] = lineOfFile.split("==");
				int count = Integer.parseInt(splitLine[1]);
				twoDayBeforeExceptionsMap.put(splitLine[0], count);
			}
			fromFile.close();
			getOneDayBeforeLogs(twoDayBeforeExceptionsMap);

		} else if (!f.exists()) {
			long start, end;
			int total;
			HashMap<String, Integer> twoDayBeforeExceptionsMap = new HashMap<String, Integer>();

			// (Map.Entry<String, String> entry : ((LinkedHashMap<String,
			// String>)
			// className.getField("queryMap").get(className)).entrySet()) {
			start = System.currentTimeMillis();
			JSONObject twoDayBeforeLogs = logsUtil.getZohoLogs(url, twoDayBeforeDate, Util.encode(query), startTime,
					endTime);
			end = System.currentTimeMillis();
			total = (int) ((end - start) * 0.001);

			twoDayBeforeExceptionsMap = parseLogs(twoDayBeforeLogs);

			getOneDayBeforeLogs(twoDayBeforeExceptionsMap);

			// }
		}
	}

	/*
	 * parse the logs and returns hashmap which contains exceptions and its
	 * count.
	 */
	public HashMap<String, Integer> parseLogs(JSONObject logs) throws Exception {
		// String exceptionBeforeSplit = "";

		JSONArray jsonArray = new JSONArray();
		String exceptionAfterSplit = "";
		HashMap<String, Integer> exceptionsAndCountMap = new HashMap<String, Integer>();

		String firstSplit[] = new String[1000];
		String secondSplit[] = new String[1000];
		String thirdSplit[] = new String[1000];

		try {
			JSONArray keys = logs.names();
			for (int i = 0; i < keys.length(); ++i) {

				String key = keys.getString(i);
				if (key.equals(Conf.resultKey)) {
					jsonArray = logs.getJSONArray(key);

				}
			}

			totalExceptionInLogServer = jsonArray.length();
			for (int i = 0; i < totalExceptionInLogServer; i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String name = jsonObject.getString("message");
				if (name.equals("crmmailsearchproblem :::")) { // temporary
																// check
					continue;
				}
				if (name.contains(":::")) {
					firstSplit = name.split(":::");
				} else if (name.contains("::")) {
					firstSplit = name.split("::");

				} else if (name.contains(":")) {
					firstSplit = name.split(":");

				} else {
					continue;
				}
				secondSplit = firstSplit[1].split("zuid");
				if (secondSplit[0].contains(":")) {
					thirdSplit = secondSplit[0].split(":");
					exceptionAfterSplit = thirdSplit[0];
				} else {
					exceptionAfterSplit = secondSplit[0];
				}
				if (exceptionsAndCountMap.containsKey(exceptionAfterSplit)) {
					exceptionsAndCountMap.put(exceptionAfterSplit, exceptionsAndCountMap.get(exceptionAfterSplit) + 1);
				} else {
					exceptionsAndCountMap.put(exceptionAfterSplit, 1);
				}
				secondSplit = null;
				firstSplit = null;

			}
		} catch (Exception e) {
			throw e;
		}
		return exceptionsAndCountMap;
	}

	/*
	 * gets onedayb4 logs from the log server. compares with the twodayb4logs
	 * and splits into old and new exceptions.
	 */
	public void getOneDayBeforeLogs(HashMap<String, Integer> fromTwoDayBefore) throws Exception {
		String oneDayBeforeDate = "";
		if (oneDayBeforeDateFromFile.equals("0")) {
			oneDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.subtarctor);
		} else {
			oneDayBeforeDate = oneDayBeforeDateFromFile;
		}
		HashMap<String, Integer> oneDayBeforeExceptionsMap = new HashMap<String, Integer>();
		HashMap<String, Integer> newExceptionsMap = new HashMap<String, Integer>();
		HashMap<String, Integer> oldExceptionsMap = new HashMap<String, Integer>();
		long start, end;
		int total;
		// Getting oneDayB4 logs from log server
		// for (Map.Entry<String, String> entry : ((LinkedHashMap<String,
		// String>) className.getField("queryMap").get(className)).entrySet()) {
		start = System.currentTimeMillis();
		JSONObject oneDayBefore = logsUtil.getZohoLogs(url, oneDayBeforeDate, Util.encode(query), startTime, endTime);
		end = System.currentTimeMillis();
		total = (int) ((end - start) * 0.001);

		oneDayBeforeExceptionsMap = parseLogs(oneDayBefore);

		// }

		// Storing onedayB4 logs in file
		StringBuilder oneDayBeforeExceptions = new StringBuilder();
		for (Map.Entry<String, Integer> m : oneDayBeforeExceptionsMap.entrySet()) {

			oneDayBeforeExceptions.append(m.getKey() + "==" + m.getValue() + "\n");

		}

		String date[] = oneDayBeforeDate.split("/");
		String saveOneDayBefore = date[0] + date[1] + date[2];

		File file = new File(filePath + saveOneDayBefore + ".txt");
		file.createNewFile();
		FileWriter writer = new FileWriter(file);
		writer.write(oneDayBeforeExceptions.toString());
		writer.close();

		// splitting in to old & new exceptions
		int totalException = 0;
		for (Map.Entry<String, Integer> m : oneDayBeforeExceptionsMap.entrySet()) {
			totalException = totalException + (Integer) m.getValue();
			String oneDayBeforeKey = m.getKey().toString();
			for (Map.Entry<String, Integer> mb : fromTwoDayBefore.entrySet()) {
				String twoDayBeforeKey = mb.getKey().toString();

				if (oneDayBeforeKey.equalsIgnoreCase(twoDayBeforeKey)) {

					oldExceptionsMap.put(oneDayBeforeKey, (Integer) m.getValue());
					oneDayBeforeExceptionsMap.put(oneDayBeforeKey, 0);

				}

			}

		}

		for (Map.Entry<String, Integer> m : oneDayBeforeExceptionsMap.entrySet()) {

			int oneDayB4Values = (Integer) m.getValue();
			if (oneDayB4Values != 0) {

				newExceptionsMap.put((String) m.getKey(), (Integer) m.getValue());
			}
		}

		createCrmMailSearchMessage(oneDayBeforeDate, totalException, oldExceptionsMap, newExceptionsMap);

	}

	/*
	 * prepares the table from the old and new exceptions hashmap and sends to
	 * sendmail method.
	 */
	public void createCrmMailSearchMessage(String oneDayBeforeDate, int totalException,
			HashMap<String, Integer> oldExceptionsMap, HashMap<String, Integer> newExceptionsMap) throws Exception {
		StringBuilder tableNewException = new StringBuilder();
		StringBuilder tableOldException = new StringBuilder();
		StringBuilder tableDateCount = new StringBuilder();
		String newExceptionsReport = "";
		int newExceptionsCount = 0;
		String oldExceptionsReport = "";
		int oldExceptionsCount = 0;
		String dateAndCount = "";

		// tableDateCount.append("<table border=1><tr><td
		// style=\"color:blue\">Duration: " + oneDayBeforeDate + " " + startTime
		// + " " + oneDayBeforeDate + " " + endTime + "</td></tr><tr><td
		// style=\"color:blue\">Total Exceptions Found: " + totalException +
		// "</td></tr></table><br><br></body></html>");
		tableDateCount.append("<table border=1><tr><td style=\"color:blue\">Duration: " + oneDayBeforeDate + " "
				+ startTime + " " + oneDayBeforeDate + " " + endTime
				+ "</td></tr><tr><td style=\"color:blue\">Total Exceptions in LogServer: " + totalExceptionInLogServer
				+ "</td></tr><tr><td style=\"color:blue\">Total Exceptions Found: " + totalException
				+ "</td></tr></table><br><br></body></html>");
		dateAndCount = tableDateCount.toString();

		tableOldException.append("<html><head>\n"
				+ "<body><table border=1 cellpadding=2 cellspacing=0><tr><th>Old Exceptions</th><th>Count</th>");

		for (Map.Entry<String, Integer> m : oldExceptionsMap.entrySet()) {
			tableOldException.append("<tr>");
			tableOldException.append("<td>" + m.getKey() + "</td>");
			tableOldException.append("<td>" + m.getValue() + "</td");
			tableOldException.append("</tr>");
			oldExceptionsCount = oldExceptionsCount + ((Integer) m.getValue());
		}
		tableOldException.append("</table><br><br></body></html>");
		oldExceptionsReport = tableOldException.toString();

		tableNewException.append("<html><head>\n" + "<body>"
				+ "<table border=1 cellpadding=2 cellspacing=0><tr><th>New Exceptions</th><th>Count</th>");

		for (Map.Entry<String, Integer> m : newExceptionsMap.entrySet()) {
			tableNewException.append("<tr>");
			tableNewException.append("<td style=\"color:red\">" + m.getKey() + "</td>");
			tableNewException.append("<td style=\"color:red\">" + m.getValue() + "</td");
			tableNewException.append("</tr>");
			newExceptionsCount = newExceptionsCount + ((Integer) m.getValue());
		}
		tableNewException.append("</table><br><br></body></html>");
		newExceptionsReport = tableNewException.toString();

		crmMailSearchDateAndcount = dateAndCount;
		crmMailSearchOldExceptionsCount = oldExceptionsCount;
		crmMailSearchOldExceptions = oldExceptionsReport;
		crmMailSearchNewExceptionsCount = newExceptionsCount;
		crmMailSearchNewExceptions = newExceptionsReport;

		////////////////////////////////////////////////////////////////////////////////
		// sendMail();
		////////////////////////////////////////////////////////////////////////////////

		folderCreate();
		// sending stats via java mail
		// // sendMail(oldExceptionsCount, oldExceptionsReport,
		// newExceptionsCount, newExceptionsReport, dateAndCount);

	}

	public void folderCreate() throws Exception {
		try {

			JSONArray jsonArray = new JSONArray();
			StringBuilder folderCreate = new StringBuilder();
			long start, end;
			int total;

			JSONObject oneDayBefore;
			String oneDayBeforeDate = "";
			if (oneDayBeforeDateFromFile.equals("0")) {
				oneDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.subtarctor);
			} else {
				oneDayBeforeDate = oneDayBeforeDateFromFile;
			}
			start = System.currentTimeMillis();
			oneDayBefore = logsUtil.getZohoLogs(url, oneDayBeforeDate, Util.encode(folderCreateQuery), startTime,
					endTime);
			end = System.currentTimeMillis();
			total = (int) ((end - start) * 0.001);
			int formatOneFound = 0;
			int formatTwoFound = 0;

			int formatNotFound = 0;
			int totalNumFound = 0;

			JSONArray keys = oneDayBefore.names();
			for (int i = 0; i < keys.length(); ++i) {

				String key = keys.getString(i);
				if (key.equals(Conf.resultKey)) {
					jsonArray = oneDayBefore.getJSONArray(key);
				}
			}

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String name = jsonObject.getString("message");
				if (name.contains(folderCreateFormat[0])) {
					formatOneFound = formatOneFound + 1;
				} else if (name.contains(folderCreateFormat[1])) {
					formatTwoFound = formatTwoFound + 1;

					String wordToFind = "mailDomain:";
					int index = name.indexOf(wordToFind);
					String mailDomain = name.substring(index + 11, name.length());

					if (mailDomainMap.containsKey(mailDomain)) {
						int previousValue = (int) mailDomainMap.get(mailDomain);
						mailDomainMap.put(mailDomain, previousValue + 1);
					} else {
						mailDomainMap.put(mailDomain, 1);
					}
				} else {
					formatNotFound = formatNotFound + 1;
				}
			}
			totalNumFound = formatOneFound + formatTwoFound + formatNotFound;

			folderCreate.append("<html><head>\n"
					+ "<body><table border=1 cellpadding=2 cellspacing=0><tr><th colspan=\"2\">Folder Create</th></tr><tr><td>"
					+ "Gmail/All Mail" + "</td><td>" + formatOneFound + "</td></tr><tr><td>" + "INBOX" + "</td><td>"
					+ formatTwoFound + "</td></tr><tr><td>Others</td><td>" + formatNotFound
					+ "</td></tr><tr><th>Total</th><th>" + totalNumFound + "</th></tr></table><br><br></body></html>");
			folderCreateMessage = folderCreate.toString();

			mailDomainMap = mailDomainMap.entrySet().stream().sorted(Entry.comparingByValue())
					.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
			List<String> mailDomainList = mailDomainMap.keySet().stream().collect(Collectors.toList());
			int size = mailDomainList.size();

			inboxMailDomainMessage.append("<html><head>\n"
					+ "<body><table border=1 cellpadding=2 cellspacing=0><tr><th colspan=\"2\">Folder Create Inbox MailDomain</th></tr>");
			for (int i = size - 1; i >= 0; i--) {
				String key = mailDomainList.get(i);
				int value = (int) mailDomainMap.get(key);
				inboxMailDomainMessage.append("<tr><td>" + key + "</td><td>" + value + "</tr>");
			}
			inboxMailDomainMessage.append("</table><br><br></body></html>");

			massMail();
		} catch (Exception e) {
			throw e;
		}

	}

	public void massMail() throws UnsupportedEncodingException, Exception {
		long start, end;
		int total;
		StringBuilder massMail = new StringBuilder();
		int oneDayBefore;
		String oneDayBeforeDate = "";
		if (oneDayBeforeDateFromFile.equals("0")) {
			oneDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.subtarctor);
		} else {
			oneDayBeforeDate = oneDayBeforeDateFromFile;
		}
		start = System.currentTimeMillis();
		oneDayBefore = logsUtil.getNumFound(url, oneDayBeforeDate, Util.encode(massMailQuery), startTime, endTime);
		end = System.currentTimeMillis();
		total = (int) ((end - start) * 0.001);
		massMail.append("<table border=1 cellpadding=2 cellspacing=0><tr><th>Mass Mail</th></tr><tr><td>Count:"
				+ oneDayBefore + "</td></tr></table><br><br>");
		massMailMessage = massMail.toString();
		topUsers();
	}

	public void topUsers() throws UnsupportedEncodingException, Exception {
		JSONArray jsonArray = new JSONArray();
		long start, end;
		int total;
		StringBuilder topUsers = new StringBuilder();
		int idArray[] = new int[100];
		int countArray[] = new int[100];
		int incrementArray = 0;

		JSONObject oneDayBefore;
		String oneDayBeforeDate = "";
		if (oneDayBeforeDateFromFile.equals("0")) {
			oneDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.subtarctor);
		} else {
			oneDayBeforeDate = oneDayBeforeDateFromFile;
		}
		start = System.currentTimeMillis();
		oneDayBefore = logsUtil.topFiveUsersTable(oneDayBeforeDate, Util.encode(topUsersNewMailQuery), startTime,
				endTime);
		end = System.currentTimeMillis();
		total = (int) ((end - start) * 0.001);

		try {
			JSONArray keys = oneDayBefore.names();
			for (int i = 0; i < keys.length(); ++i) {

				String key = keys.getString(i);
				if (key.equals(Conf.topUsersKey)) {
					jsonArray = oneDayBefore.getJSONArray(key);
				}
			}

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				int id = (Integer) jsonObject.get("groupby(_c_currzuid)");
				int count = (Integer) jsonObject.get("count");
				idArray[incrementArray] = id;
				countArray[incrementArray] = count;
				incrementArray++;

			}

			topUsers.append("<html><head>\n"
					+ "<body><table border=1 cellpadding=2 cellspacing=0><tr><th colspan=\"2\">Top Users New Mail</th></tr><tr><th>Id</th><th>Count</th>");

			for (int i = 0; i < incrementArray; i++) {
				topUsers.append("<tr>");
				topUsers.append("<td>" + idArray[i] + "</td>");
				topUsers.append("<td>" + countArray[i] + "</td>");
				topUsers.append("</tr>");
			}
			topUsers.append("</table><br><br></body></html>");

			topUsersMessage = topUsers.toString();
			mailWithoutContent();

		} catch (Exception e) {
			throw e;
		}

	}

	public void mailWithoutContent() throws UnsupportedEncodingException, Exception {

		JSONArray jsonArray = new JSONArray();
		long start, end;
		int total;
		int numFound;
		String numFoundMessage;
		StringBuilder mailWihtoutContent = new StringBuilder();
		int idArray[] = new int[100];
		int countArray[] = new int[100];
		int incrementArray = 0;

		JSONObject oneDayBefore;
		String oneDayBeforeDate = "";
		if (oneDayBeforeDateFromFile.equals("0")) {
			oneDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.subtarctor);
		} else {
			oneDayBeforeDate = oneDayBeforeDateFromFile;
		}
		start = System.currentTimeMillis();
		oneDayBefore = logsUtil.topFiveUsersTable(oneDayBeforeDate, Util.encode(mailWithoutContentQuery), startTime,
				endTime);
		end = System.currentTimeMillis();
		total = (int) ((end - start) * 0.001);

		try {
			JSONArray keys = oneDayBefore.names();
			for (int i = 0; i < keys.length(); ++i) {

				String key = keys.getString(i);
				if (key.equals(Conf.topUsersKey)) {
					jsonArray = oneDayBefore.getJSONArray(key);

				}
			}

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				int id = (Integer) jsonObject.get("groupby(_c_currzuid)");
				int count = (Integer) jsonObject.get("count");
				idArray[incrementArray] = id;
				countArray[incrementArray] = count;
				incrementArray++;

			}

			mailWihtoutContent.append("<html><head>\n"
					+ "<body><table border=1 cellpadding=2 cellspacing=0><tr><th colspan=\"3\">Mail Without Content</th></tr><tr><th>Id</th><th>Count</th><th>TotalNewMail</th></tr>");

			start = System.currentTimeMillis();

			for (int i = 0; i < incrementArray; i++) {
				mailWihtoutContent.append("<tr>");
				numFoundMessage = mailWithoutContentQuery2a + idArray[i] + mailWithoutContentQuery2b;
				mailWihtoutContent.append("<td>" + idArray[i] + "</td>");
				mailWihtoutContent.append("<td>" + countArray[i] + "</td>");
				numFound = logsUtil.getNumFound(url, oneDayBeforeDate, Util.encode(numFoundMessage), startTime,
						endTime);

				mailWihtoutContent.append("<td>" + numFound + "</td>");

				mailWihtoutContent.append("</tr>");
				numFound = 0;
				numFoundMessage = "";
			}
			mailWihtoutContent.append("</table><br><br></body></html>");

			mailWithoutContentMessage = mailWihtoutContent.toString();
			end = System.currentTimeMillis();
			total = (int) ((end - start) * 0.001);

			seTypeChange();

		} catch (Exception e) {
			throw e;
		}

	}

	public void seTypeChange() throws UnsupportedEncodingException, Exception {

		long start, end;
		int total;
		StringBuilder seTypeChange = new StringBuilder();
		int oneDayBefore;
		String oneDayBeforeDate = "";
		if (oneDayBeforeDateFromFile.equals("0")) {
			oneDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.subtarctor);
		} else {
			oneDayBeforeDate = oneDayBeforeDateFromFile;
		}
		start = System.currentTimeMillis();
		oneDayBefore = logsUtil.getNumFound(url, oneDayBeforeDate, Util.encode(seTypeChangeQuery), startTime, endTime);
		end = System.currentTimeMillis();
		total = (int) ((end - start) * 0.001);
		seTypeChange.append("<table border=1 cellpadding=2 cellspacing=0><tr><th>SeTypeChange</th></tr><tr><td>Count:"
				+ oneDayBefore + "</td></tr></table><br><br>");
		seTypeChangeMessage = seTypeChange.toString();
		topUsersFailed();

	}

	public void topUsersFailed() throws UnsupportedEncodingException, Exception {

		JSONArray jsonArray = new JSONArray();
		long start, end;
		int total;
		String firstSplit[] = new String[100];
		String zuid[] = new String[100];
		String totalRecords[] = new String[100];
		HashMap<String, Integer> topUsersFailedMap = new HashMap<String, Integer>();
		StringBuilder topUsersFailed = new StringBuilder();

		JSONObject oneDayBefore;
		String oneDayBeforeDate = "";
		if (oneDayBeforeDateFromFile.equals("0")) {
			oneDayBeforeDate = Util.getFormattedDate(Conf.Logs_Date_Format, Conf.subtarctor);
		} else {
			oneDayBeforeDate = oneDayBeforeDateFromFile;
		}
		start = System.currentTimeMillis();
		oneDayBefore = logsUtil.getZohoLogs(url, oneDayBeforeDate, Util.encode(topUsersFailedQuery), startTime,
				endTime);
		end = System.currentTimeMillis();
		total = (int) ((end - start) * 0.001);
		File file = new File("/.txt");
		file.createNewFile();
		FileWriter writer = new FileWriter(file);
		writer.write(oneDayBefore.toString());
		writer.close();

		try {
			JSONArray keys = oneDayBefore.names();
			for (int i = 0; i < keys.length(); ++i) {

				String key = keys.getString(i);
				if (key.equals(Conf.resultKey)) {
					jsonArray = oneDayBefore.getJSONArray(key);
				}
			}

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String name = jsonObject.getString("message");
				firstSplit = name.split(" ");
				for (int j = 0; j < firstSplit.length; j++) {
					if (firstSplit[j].contains("zuid")) {
						zuid = firstSplit[j].split("zuid:");
					} else if (firstSplit[j].contains("totalRecords")) {
						totalRecords = firstSplit[j].split("totalRecords:");
					}
				}

				int totalRecordsCount = Integer.parseInt(totalRecords[1]);

				if (topUsersFailedMap.containsKey(zuid[1])) {
					topUsersFailedMap.put(zuid[1], topUsersFailedMap.get(zuid[1]) + totalRecordsCount);
				} else {
					topUsersFailedMap.put(zuid[1], totalRecordsCount);
				}

				firstSplit = null;
				zuid = null;
				totalRecords = null;

			}
		} catch (Exception e) {
			throw e;
		}

		topUsersFailed.append("<html><head>\n"
				+ "<body><table border=1 cellpadding=2 cellspacing=0><tr><th colspan=\"2\">Top 5 Users Failed</th></tr><tr><th>Zuid</th><th>Total</th></tr>");

		Map<String, Integer> map = sortByValues(topUsersFailedMap);
		ArrayList<String> keyList = new ArrayList<String>(map.keySet());
		int topFiveCount = 0;
		for (int i = keyList.size() - 1; i >= 0; i--) {
			// get key
			String key = (String) keyList.get(i);
			int value = map.get(key);
			topFiveCount += 1;
			if (topFiveCount <= 5) {

				topUsersFailed.append("<tr>");
				topUsersFailed.append("<td>" + key + "</td>");
				topUsersFailed.append("<td>" + value + "</td>");

				topUsersFailed.append("</tr>");

			}

		}
		topUsersFailed.append("</table><br><br></body></html>");

		topUsersFailedMessage = topUsersFailed.toString();

		sendMail();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static HashMap<String, Integer> sortByValues(HashMap map) {
		List list = new LinkedList(map.entrySet());
		// Defined Custom Comparator here
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		// Here I am copying the sorted list in HashMap
		// using LinkedHashMap to preserve the insertion order
		HashMap<String, Integer> sortedHashMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedHashMap.put((String) entry.getKey(), (int) entry.getValue());
		}
		return sortedHashMap;
	}

	

	/*
	 * send the mail to the required mail id.
	 */
	public void sendMail() throws AddressException, MessagingException {

		final String username = "";
		final String password = "";

		StringBuilder finalEmail = new StringBuilder();
		finalEmail.append(crmMailSearchDateAndcount);
		finalEmail.append("Old Exceptions Count:" + crmMailSearchOldExceptionsCount);
		finalEmail.append(crmMailSearchOldExceptions);
		finalEmail.append("New Exceptions Count:" + crmMailSearchNewExceptionsCount);
		finalEmail.append(crmMailSearchNewExceptions);
		finalEmail.append(folderCreateMessage);
		finalEmail.append(inboxMailDomainMessage.toString());
		finalEmail.append(massMailMessage);
		finalEmail.append(seTypeChangeMessage);
		finalEmail.append(topUsersMessage);
		finalEmail.append(mailWithoutContentMessage);
		finalEmail.append(topUsersFailedMessage);

		
		Send(username, password, "", "", "", finalEmail.toString());

	}

	public static void Send(final String username, final String password, String recipientEmail, String ccEmail,
			String title, String message) throws AddressException, MessagingException {
		// Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		final String SSL_FACTORY = "";

		// Get a Properties object
		Properties props = System.getProperties();
		props.setProperty("mail.smtps.host", "");
		props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "");
		props.setProperty("mail.smtp.socketFactory.port", "");
		props.setProperty("mail.smtps.auth", "");

		/*
		 * If set to false, the QUIT command is sent and the connection is
		 * immediately closed. If set to true (the default), causes the
		 * transport to wait for the response to the QUIT command.
		 * 
		 * ref :
		 * http://java.sun.com/products/javamail/javadocs/com/sun/mail/smtp/
		 * package-summary.html
		 * http://forum.java.sun.com/thread.jspa?threadID=5205249 smtpsend.java
		 * - demo program from javamail
		 */
		props.put("mail.smtps.quitwait", "");

		Session session = Session.getInstance(props, null);

		// -- Create a new message --
		final MimeMessage msg = new MimeMessage(session);

		// -- Set the FROM and TO fields --
		msg.setFrom(new InternetAddress(username + ""));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));

		if (ccEmail.length() > 0) {
			msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
		}

		msg.setSubject(title);
		msg.setContent(message, "text/html");
		msg.setSentDate(new Date());

		SMTPTransport t = (SMTPTransport) session.getTransport("");

		t.connect("", username, password);
		t.sendMessage(msg, msg.getAllRecipients());
		t.close();
	}
}
