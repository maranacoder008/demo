package com.zoho.logs;
//$Id$

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Util {

    public static String getFormattedDate(String format, int subtractor) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, subtractor);
        return dateFormat.format(cal.getTime());
    }

    public static String encode(String data) throws UnsupportedEncodingException {
        return URLEncoder.encode(data, "UTF-8");
    }

}
