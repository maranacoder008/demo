package com.zoho.logs;
//$Id$

public interface Conf {

    int subtarctor = -1;
    int twoDaySubtractor = -2;
    String resultKey = "";
    String topUsersKey = "";

    String LogsAuthtoken = "";
    String LogsAppId = "";

    String ChatAuthtoken = "";
    String ChannelName = "";
 
    String ReportsAuthtoken = "";
    String LOGINEMAILID = "";
    String DATABASENAME = "";

    String Logs_Date_Format = "";
    String Reports_Date_Format = "";
    String Readable_Date_Format = "";
}
